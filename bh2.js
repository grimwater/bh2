import { BH2 } from "./modules/config.js";

Hooks.once("init", function () {
  console.log(`Initializing \n${BH2.AsciiTitle}`);

  game.bh2 = {
    config: BH2
  };
});

Hooks.once("setup", function () {});

Hooks.once("ready", function () {});
