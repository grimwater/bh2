$contents = Get-Content ./system.json -Raw
$contents -match '"version": "(\d+)\.(\d+)"'
$majorVersion = [int]$matches[1]
$minorVersion = [int]$matches[2] + 1
($contents `
  -replace '("version": "\d+)\.(\d+)"', "`$1.$minorVersion""" `
  -replace '("download": "https://gitlab\.com/grimwater/bh2/raw/main/)(.*)\.zip"', "`$1distro-$majorVersion.$minorVersion.zip""").Trim() `
  | Out-File ./system.json

Move-Item ./*.zip -Destination old
Compress-Archive -Path . -DestinationPath "distro-$majorVersion.$minorVersion.zip" -Force
